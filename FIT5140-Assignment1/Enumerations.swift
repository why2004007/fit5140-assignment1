//
//  Enumerations.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation

// Enum : Notification Range
enum NotificationRange : Int {
    case Near = 50, Moderate = 250, Far = 1000
}

// This enum is designed to convert nsmanaged bool type into Bool
enum NSManagedBool : Int {
    case True = 1, False = 0
}

// This enum is one to one maped to the color segment
enum TitleColor : String {
    case Black = "Black", Cyan = "Cyan", Brown = "Brown", Red = "Red", Green = "Green"
}