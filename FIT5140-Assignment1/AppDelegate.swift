//
//  AppDelegate.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import MapKit

protocol LocationUpdatedDelegate {
    func locationUpdated(sender: AppDelegate, locationIsUpdated location: CLLocation)
}

protocol CategoryListUpdatedDelegate {
    func categoryListUpdated()
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, CategoryListControllerDelegate {

    var window: UIWindow?
    var locationManager: CLLocationManager = CLLocationManager()
    var locationUpdatedDelegates = [LocationUpdatedDelegate]()
    var categoryListUpdatedDelegates = [CategoryListUpdatedDelegate]()
    var categoryList: [Category]!
    var currentLocation: CLLocation!
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Request permission for location manager
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        // Request permission for notifications
        let supportedNotificationTypes:UIUserNotificationType = [.Alert, .Badge, .Sound]
        let notificationSettings = UIUserNotificationSettings(forTypes: supportedNotificationTypes, categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
        
        // Set up geofencing
        setUpGeoFencing()
        
        return true
    }

    // MARK: - Custom Functions: Data Manipulating
    
    func fetchData() {
        let request = NSFetchRequest(entityName: "Category")
        let sortByIndex = NSSortDescriptor(key: "index", ascending: true)
        request.sortDescriptors = [sortByIndex]
        do {
            categoryList = try managedObjectContext.executeFetchRequest(request) as! [Category]
        } catch {
            return
        }
    }
    
    // MARK: - Custom Functions: GeoFencing
    
    func setUpGeoFencing() {
        // Fetch category data
        fetchData()
        
        // Stop all the regions
        for region in locationManager.monitoredRegions {
            locationManager.stopMonitoringForRegion(region)
        }
        
        // Sort the category list to make sure it is ordered by the distance between target location and current location
        // If list length is less than 20, do not sort list
        if categoryList.count >= 20 {
            sortCategoryListByNotificationRange()
        }
        
        // Set new locations to be monitored
        for category in categoryList! {
            if NSManagedBool(rawValue: category.needNotification! as Int) == .True {
                let center = CLLocationCoordinate2D(latitude: category.location!.latitude! as Double, longitude: category.location!.longitude! as Double)
                let region = CLCircularRegion(center: center, radius: category.notificationRange as! Double, identifier: category.title!)
                if locationManager.monitoredRegions.count < 20 {
                    locationManager.startMonitoringForRegion(region)
                } else {
                    break
                }
                
            }
        }
    }
    
    func distanceBetweenLocations(location1: CLLocation, location2: CLLocation) -> CLLocationDistance {
        return location1.distanceFromLocation(location2)
    }
    
    func generateLocation(latitude: Double, longitude: Double) -> CLLocation {
        return CLLocation(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), altitude: 0, horizontalAccuracy: 0, verticalAccuracy: 0, course: 0, speed: 0, timestamp: NSDate())
    }
    
    func sortCategoryListByNotificationRange() {
        if currentLocation == nil {
            return
        }
        categoryList = categoryList.sort({ (category1: Category, category2: Category) -> Bool in
            let locationOfCategory1 = generateLocation(category1.location!.latitude as! Double, longitude: category1.location!.longitude as! Double)
            let locationOfCategory2 = generateLocation(category2.location!.latitude as! Double, longitude: category2.location!.longitude as! Double)
            return distanceBetweenLocations(currentLocation, location2: locationOfCategory1) < distanceBetweenLocations(currentLocation, location2: locationOfCategory2)
        })
    }
    
    // MARK: - Custom Functions: Register Delegates
    
    func registerLocationUpdateDelegate(delegate: LocationUpdatedDelegate) {
        locationUpdatedDelegates.append(delegate)
    }
    
    func registerCategoryListUpdatedDelegate(delegate: CategoryListUpdatedDelegate) {
        categoryListUpdatedDelegates.append(delegate)
    }
    
    // MARK: - Delegate Functions: CategoryListController Delegate
    
    func categoryListChanged() {
        for delegate in categoryListUpdatedDelegates {
            delegate.categoryListUpdated()
        }
    }
    
    // MARK: - Delegate Functions: Location Manager
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.first!
        for delegate in locationUpdatedDelegates {
            delegate.locationUpdated(self, locationIsUpdated: locations.first!)
        }
    }
    
    func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion) {
        let title = "Region Entered"
        let message = "You have entered the region set for category \(region.identifier). Please check the reminders of it."
        if UIApplication.sharedApplication().applicationState == .Active {
            let alert = ViewController.getAlertControlloer(title, message: message)
            UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
        } else {
            let notification = UILocalNotification()
            notification.alertTitle = title
            notification.alertBody = message
            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        }
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.musicfly.FIT5140_Assignment1" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("FIT5140_Assignment1", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

