//
//  SearchResultsController.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit

protocol SearchResultsControllerDelegate {
    func searchResult(sender: SearchResultsController, didSelectResult result: GeocodeAddress)
}

class SearchResultsController: UITableViewController, SearchResultUpdaterDelegate {

    // Data array
    var results: [GeocodeAddress]?
    
    // Delegate which receives the selected location
    var delegate: SearchResultsControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data updater

    func searchResult(sender: PickUpLocationController, searchResultDidLoaded results: [GeocodeAddress]) {
        self.results = results
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // We only need one section to present results
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if results != nil {
            return results!.count
        } else {
            return 0
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchResultCell", forIndexPath: indexPath) as! SearchResultCell

        cell.addressLabel.text = results![indexPath.row].address!

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        delegate!.searchResult(self, didSelectResult: results![indexPath.row])
    }

}
