//
//  SplitViewController.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 9/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController, UISplitViewControllerDelegate {

    var toggleMasterViewButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice().userInterfaceIdiom == .Phone {
            viewControllers.removeLast()
        }
        toggleMasterViewButton = displayModeButtonItem()
        //preferredDisplayMode = .PrimaryOverlay
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func toggleMasterView() {
        UIApplication.sharedApplication().sendAction(toggleMasterViewButton.action, to: toggleMasterViewButton.target, from: nil, forEvent: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
