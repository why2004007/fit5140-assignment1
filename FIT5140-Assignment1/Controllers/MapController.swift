//
//  MapController.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class MapController: UIViewController, MKMapViewDelegate, LocationUpdatedDelegate, CategoryListUpdatedDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    // Store fetched categories
    var categoryList: [Category]!
    
    // A reference to AppDelegate
    let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // Set up a flag to determine whether the view is first loaded or not
    var isInit: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Display user location on map
        mapView.showsUserLocation = true
        
        // Register category list changed delegates
        appDelegate.registerCategoryListUpdatedDelegate(self)
        
        // Register location changed delegates
        appDelegate.registerLocationUpdateDelegate(self)
        
        // Do any additional setup after loading the view.
        let displayModeButtonItem = splitViewController?.displayModeButtonItem()
        navigationItem.leftBarButtonItem = displayModeButtonItem
        navigationItem.leftItemsSupplementBackButton = true
        
        // Set self as mapview delegate
        mapView.delegate = self
        
        // Fetch the category list
        fetchData()
        
        // Display the categories as annotations
        updateAnnotations()
        
        // Display the notification range for annotations
        updateOverlays()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Functions: Data Manipulating
    
    func fetchData() {
        let request = NSFetchRequest(entityName: "Category")
        let sortByIndex = NSSortDescriptor(key: "index", ascending: true)
        request.sortDescriptors = [sortByIndex]
        do {
            categoryList = try appDelegate.managedObjectContext.executeFetchRequest(request) as! [Category]
        } catch {
            let alert = ViewController.getAlertControlloer("Error", message: "Failed to load the stored data.")
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Custom Functions: View Controll
    
    func updateAnnotations() {
        // First, remove annotations from mapview
        mapView.removeAnnotations(mapView.annotations)
        
        
        
        // Add new annotations to mapview
        for category in categoryList! {
            // Do not add annotation for category which does not want to receive notifications
            if NSManagedBool(rawValue: category.needNotification! as Int) == .False {
                continue
            }
            let location = category.location!
            let annotation = MyPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude! as Double, longitude: location.longitude! as Double)
            annotation.title = category.title!
            annotation.category = category
            mapView.addAnnotation(annotation)
        }
    }
    
    func updateOverlays() {
        // First, remove all the overlays from mapview
        mapView.removeOverlays(mapView.overlays)
        // Add new overlays to mapview
        for category in categoryList! {
            // Do not add overlay for category which does not want to receive notifications
            if NSManagedBool(rawValue: category.needNotification! as Int) == .False {
                continue
            }
            let location = category.location!
            let coordinate = CLLocationCoordinate2D(latitude: location.latitude! as Double, longitude: location.longitude! as Double)
            let overlay = MKCircle(centerCoordinate: coordinate, radius: category.notificationRange! as Double)
            mapView.addOverlay(overlay)
        }
    }
    
    func changeRegion(coordinate: CLLocationCoordinate2D) {
        // Change current view point to specific coordinate
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(coordinate, span)
        mapView.setRegion(region, animated: true)
    }
    
    // MARK: - Delegate Functions: CategoryListChanged Delegate
    
    func categoryListUpdated() {
        fetchData()
        updateAnnotations()
        updateOverlays()
    }
    
    // MARK: - Delegate Functions: Map View Delegate
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        // Customize the annotation view
        if annotation.isEqual(mapView.userLocation) {
            return nil
        }
        
        let view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
        view.animatesDrop = true
        view.canShowCallout = true
        let button = UIButton(type: UIButtonType.DetailDisclosure)
        button.addTarget(self, action: #selector(calloutButtonClicked), forControlEvents: UIControlEvents.TouchUpInside)
        view.rightCalloutAccessoryView = button
        return view
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        // Customize the overlay
        let renderer = MKCircleRenderer(overlay: overlay)
        renderer.fillColor = UIColor.purpleColor()
        renderer.alpha = 0.5
        return renderer
    }
    
    // MARK: - Custom Functions: Button Events
    
    func calloutButtonClicked() {
        // Based on the annotation clicked, set up the destination view
        let annotation = mapView.selectedAnnotations.first! as! MyPointAnnotation
        let category = annotation.category!
        let destination = storyboard?.instantiateViewControllerWithIdentifier("ReminderListController") as! ReminderListController
        destination.reminders = Array(category.reminders!)
        destination.category = category
        
        // Manually handle the navigation between master view and detail view
        let masterNavController = splitViewController?.viewControllers.first! as! UINavigationController
        masterNavController.pushViewController(destination, animated: true)
        
        // Open the master view when the device is pad
        if UIDevice().userInterfaceIdiom == .Pad {
            let customSplitViewController = splitViewController! as! SplitViewController
            customSplitViewController.toggleMasterView()
        }
    }
    
    // MARK: - Delegate Functions: Location Updated Delegate
    
    func locationUpdated(sender: AppDelegate, locationIsUpdated location: CLLocation) {
        if isInit {
            isInit = false
            changeRegion(location.coordinate)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
