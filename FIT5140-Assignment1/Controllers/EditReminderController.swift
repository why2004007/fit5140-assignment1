//
//  EditReminderController.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit

protocol EditReminderControllerDelegate {
    func reminderUpdated(sender: EditReminderController, reminderIsUpdated existingReminder: Reminder)
    func reminderUpdated(sender: EditReminderController, reminderIsCreated newReminder: Reminder)
}

class EditReminderController: UIViewController, ReminderStateChangedDelegate {

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var dueDateField: UITextField!
    @IBOutlet weak var finishedSwitch: UISwitch!
    @IBOutlet weak var noteField: UITextView!
    
    // Add a reference to AppDelegate
    var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // Variables to store whether is editing existing reminder
    var isEditingExistingReminder: Bool = false
    var reminder: Reminder?
    
    // Data formatter to format date into string
    let dateFormatter: NSDateFormatter = NSDateFormatter()
    
    // Selected due date
    var selectedDueDate: NSDate!
    
    // Delegate to receive the reminder is updated information
    var delegate: EditReminderControllerDelegate?
    
    // Use datepicker to help user select due date
    var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialise date formatter
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        // Load content of working reminder
        if isEditingExistingReminder {
            loadContent()
        }
        
        // Set up button events
        setUpButtonEvent()
        
        // Set up datepicker
        datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePickerMode.DateAndTime
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
        
        // Set datepicker as duedate text field's inputview
        dueDateField.inputView = datePicker
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Functions: View Controll
    
    func setUpButtonEvent() {
        saveButton.target = self
        saveButton.action = #selector(saveButtonClicked)
        
        cancelButton.target = self
        cancelButton.action = #selector(cancelButtonClicked)
    }
    
    func saveButtonClicked() {
        // Perform input validation
        let alert = inputValidation()
        if alert != nil {
            presentViewController(alert!, animated: true, completion: nil)
            return
        }
        
        // Judge whether we are editing existing reminder
        if isEditingExistingReminder {
            
            // Set title
            reminder!.title = titleField.text!
            
            // Due date is an optional part
            if selectedDueDate != nil {
                reminder!.dueDate = selectedDueDate
            }
            
            // Convert Bool to NSManagedBool
            if finishedSwitch.on {
                reminder!.finished = NSManagedBool.True.rawValue
            } else {
                reminder!.finished = NSManagedBool.False.rawValue
            }
            
            // Note is optional part
            if noteField.text! == "" {
                reminder!.note = nil
            } else {
                reminder!.note = noteField.text!
            }
            
            delegate!.reminderUpdated(self, reminderIsUpdated: reminder!)
            
        } else {
            
            // Set title
            let title = titleField.text!
            
            // Note is optional part
            var note: String! = nil
            if noteField.text != "" {
                note = noteField.text!
            }
            
            // Convert Bool to NSManagedBool
            var finishedState: NSManagedBool
            if finishedSwitch.on {
                finishedState = NSManagedBool.True
            } else {
                finishedState = NSManagedBool.False
            }
            
            // Create new reminder by given parameters
            let newReminder = ReminderListController.createReminder(appDelegate, title: title, finished: finishedState, dueDate: selectedDueDate, note: note)
            
            delegate!.reminderUpdated(self, reminderIsCreated: newReminder)
        }
        navigationController?.popViewControllerAnimated(true)
    }
    
    func cancelButtonClicked() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func loadContent() {
        titleField.text = reminder!.title
        if reminder!.finished == NSManagedBool.False.rawValue {
            finishedSwitch.on = false
        } else {
            finishedSwitch.on = true
        }
        if reminder!.dueDate != nil {
            dueDateField.text = dateFormatter.stringFromDate(reminder!.dueDate!)
            // Manually set due date based on the field of reminder
            selectedDueDate = reminder!.dueDate
        }
        if reminder!.note != nil {
            noteField.text = reminder!.note!
        }
    }
    
    func datePickerValueChanged() {
        // Convert NSDate to text and assign it to due date field
        dueDateField.text = dateFormatter.stringFromDate(datePicker.date)
        // Assign selected date to selectedDueDate
        selectedDueDate = datePicker.date
    }
    
    // MARK: - Custom Functions: Data Manipulating

    func inputValidation() -> UIAlertController? {
        var checkNotPass = false
        var message = "You must fill up "
        if titleField.text! == "" {
            message = "the title"
            checkNotPass = true
        }
        message += "."
        if checkNotPass {
            return ViewController.getAlertControlloer("Error", message: message)
        } else {
            return nil
        }
    }
    
    // MARK: - Custom Functions: Reminder State Changed Delegate
    
    func reminderStateChanged(sender: ReminderListController, reminderStateIsChanged newState: Bool) {
        finishedSwitch.on = newState
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
