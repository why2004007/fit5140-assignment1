//
//  CategoryListController.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit
import CoreData

protocol CategoryListControllerDelegate {
    func categoryListChanged()
}

class CategoryListController: UITableViewController, EditCategoryControllerDelegate {

    @IBOutlet var addButton: UIBarButtonItem!
    @IBOutlet var editButton: UIBarButtonItem!
    @IBOutlet var mapButton: UIBarButtonItem!
    
    // A reference to navigation controller to be used for splitviewcontroller
    var navController: UINavigationController!
    
    // Manually set up a done button.
    var doneButton: UIBarButtonItem!
    
    // Set up a reference to AppDelegate
    var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // Indicator of whether this table is editing or not
    var isEditingMode: Bool = false
    
    // Working object
    var categoryList: [Category]!
    
    // Platform Info
    var platform = UIDevice().userInterfaceIdiom
    
    // Delegate to receive the category list update signal
    var delegate: CategoryListControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Assign AppDelegate as the category list update event listener
        delegate = appDelegate
        
        // Assign navigation controller to variable
        navController = self.navigationController!
        
        // We want to the table to be selectable in editing mode, so that we can edit category.
        tableView.allowsSelectionDuringEditing = true
        
        // Set up the response for edit button.
        editButton.target = self
        editButton.action = #selector(editButtonClicked)
        
        // Set up the done button for editing mode.
        doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(doneButtonClicked))
        
        // Set up the response for add button.
        addButton.target = self
        addButton.action = #selector(addButtonClicked)
        
        // Initially, we need the normal mode.
        navigationItem.leftBarButtonItem = nil
        setUpViewForNormalMode()
        
        // Fetch the data for table view
        fetchCategories()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom functions: Editing mode control
    
    func addButtonClicked() {
        // Retrieve nav controller of detail view
        let navController = splitViewController!.viewControllers.last! as! UINavigationController
        
        // Check if the add category controller is added
        let topView = navController.topViewController!
        if topView.isKindOfClass(EditCategoryController) {
            let topViewController = topView as! EditCategoryController
            if !topViewController.isEditingExistingCategory {
                return
            }
            // Pop out the view which is a sub class of EditCategoryController
            navController.popViewControllerAnimated(true)
        } else if topView.isKindOfClass(PickUpLocationController) {
            let topViewController = topView as! PickUpLocationController
            // If current view is for creating category, do not change top view
            if topViewController.category == nil {
                return
            }
            // Pop current pickuplocation view controller
            navController.popViewControllerAnimated(true)
            // Pop editcategory view controller
            navController.popViewControllerAnimated(true)
        }
        
        // Instanciate the destination view
        let destination = storyboard?.instantiateViewControllerWithIdentifier("EditCategoryController") as! EditCategoryController
        destination.delegate = self
        destination.isEditingExistingCategory = false
        destination.category = nil
        destination.categoryIndex = categoryList!.count
        
        // Push the view to detail view
        navController.pushViewController(destination, animated: true)
        
        if UIDevice().userInterfaceIdiom == .Pad {
            // Hide master view
            let customSplitViewController = splitViewController! as! SplitViewController
            customSplitViewController.toggleMasterView()
        }
    }
    
    func editButtonClicked() {
        isEditingMode = true
        setUpViewForEditingMode()
        tableView.setEditing(true, animated: true)
    }
    
    func doneButtonClicked() {
        isEditingMode = false
        setUpViewForNormalMode()
        tableView.setEditing(false, animated: true)
        
        // If I quit editing mode, pop out all the views in detail view until map view is reached
        
        // Retrieve navigation controller of detail view
        let navController = splitViewController!.viewControllers.last! as! UINavigationController
        
        // Pop out all the view until map view is the first view
        for view in navController.viewControllers {
            if !view.isKindOfClass(MapController) {
                navController.popViewControllerAnimated(true)
            }
        }
    }
    
    func mapButtonClicked() {
        splitViewController?.showDetailViewController((splitViewController?.viewControllers.last!)!, sender: self)
    }
    
    func setUpViewForEditingMode() {
        // Prepare the views when entering the editing mode
        navigationItem.setLeftBarButtonItem(addButton, animated: true)
        navigationItem.setRightBarButtonItem(doneButton, animated: true)
    }
    
    func setUpViewForNormalMode() {
        // Prepare the views when entering the normal mode
        if platform == .Phone {
            navigationItem.setLeftBarButtonItem(mapButton, animated: true)
        } else {
            navigationItem.setLeftBarButtonItem(nil, animated: true)
        }
        navigationItem.setRightBarButtonItem(editButton, animated: true)
    }
    
    // MARK: - Custom functions: Data manipulating
    
    func fetchCategories() {
        let request = NSFetchRequest(entityName: "Category")
        let sortByIndex = NSSortDescriptor(key: "index", ascending: true)
        request.sortDescriptors = [sortByIndex]
        do {
            categoryList = try appDelegate.managedObjectContext.executeFetchRequest(request) as! [Category]
        } catch {
            let alert = ViewController.getAlertControlloer("Error", message: "Failed to load the stored data.")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        if categoryList!.count == 0 {
            self.initCategoryList()
        }
        
    }
    
    static func createNewCategory(appDelegate: AppDelegate, index: Int, title: String, color: TitleColor, needNotification: NSManagedBool, notificationRange: NotificationRange, location: Location) -> Category {
        // Return a new category based on settings
        let newCategory = NSEntityDescription.insertNewObjectForEntityForName("Category", inManagedObjectContext: appDelegate.managedObjectContext)
        newCategory.setValue(index, forKey: "index")
        newCategory.setValue(title, forKey: "title")
        newCategory.setValue(color.rawValue, forKey: "color")
        newCategory.setValue(needNotification.rawValue, forKey: "needNotification")
        newCategory.setValue(notificationRange.rawValue, forKey: "notificationRange")
        newCategory.setValue(location, forKey: "location")
        newCategory.setValue(Set<Reminder>(), forKey: "reminders")
        return newCategory as! Category
        
    }

    func convertTitleColorToUIColor(color: TitleColor) -> UIColor {
        switch color {
        case .Black: return UIColor.blackColor()
        case .Brown: return UIColor.brownColor()
        case .Cyan: return UIColor.cyanColor()
        case .Green: return UIColor.greenColor()
        case .Red: return UIColor.redColor()
        }
    }
    
    func initCategoryList() {
        // Initialise the category list by two pre-set categories
        let location1 = PickUpLocationController.createLocation(appDelegate, address: "Dandenong Rd & Derby Rd Caulfield Plaza, Caulfield VIC 3162, Australia", latitude: -37.8763399, longitude: 145.0423579)
        // We do not need a variable to store the value created, because it has already been insert into the managed object context
        CategoryListController.createNewCategory(appDelegate, index: 0, title: "Shop for dinner", color: TitleColor.Red, needNotification: NSManagedBool.True, notificationRange: NotificationRange.Moderate, location: location1)
        
        let location2 = PickUpLocationController.createLocation(appDelegate, address: "280 Balaclava Rd, Caulfield North VIC 3162, Australia", latitude: -37.8718895, longitude: 145.0285579)
        CategoryListController.createNewCategory(appDelegate, index: 0, title: "Have a rest", color: TitleColor.Black, needNotification: NSManagedBool.True, notificationRange: NotificationRange.Far, location: location2)
        // Store changes
        appDelegate.saveContext()
        // Reload the data from persistent storage
        fetchCategories()
    }
    
    // MARK: - Delegate function for EditCategoryController
    
    func categoryUpdated(sender: EditCategoryController, didFinishEditing existingCategory: Category) {
        appDelegate.saveContext()
        tableView.reloadData()
        delegate.categoryListChanged()
    }
    
    func categoryUpdated(sender: EditCategoryController, didFinishCreating newCategory: Category) {
        appDelegate.saveContext()
        fetchCategories()
        tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
        delegate.categoryListChanged()
        //let indexPath = NSIndexPath(forItem: categoryList!.count, inSection: 0)
        //tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // We only have one section for the table view
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // We only have one section, so we just return the length of categoryList
        return categoryList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CategoryCell", forIndexPath: indexPath) as! CategoryCell

        let category = categoryList![indexPath.row]
        cell.titleLabel.text = category.title!
        cell.titleLabel.textColor = convertTitleColorToUIColor(TitleColor(rawValue: category.color!)!)
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */
 
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // Just implement delete style, insert will be handled by delegate
        if editingStyle == .Delete {
            appDelegate.managedObjectContext.deleteObject(categoryList[indexPath.row])
            appDelegate.saveContext()
            fetchCategories()
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            delegate.categoryListChanged()
        }
    }

    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        
        // Update row index locally
        let movedRow = categoryList[fromIndexPath.row]
        categoryList.removeAtIndex(fromIndexPath.row)
        categoryList.insert(movedRow, atIndex: toIndexPath.row)
        
        // Update the list according to the new index
        for index in 0..<categoryList.count {
            categoryList[index].index = index
        }
        // Save changes
        appDelegate.saveContext()
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return isEditingMode
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return isEditingMode
    }
    
    // MARK: - Navigation
/*
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Based on the mode, perform corresponding segue
        if isEditingMode {
            performSegueWithIdentifier("EditCategory", sender: indexPath.row)
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        // Judge whether current segue should be performed or not
        if identifier == "EditCategory" || identifier == "AddCategory" {
            return isEditingMode
        } else {
            return !isEditingMode
        }
    }*/
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Based on the mode, perform corresponding segue
        if isEditingMode {
            
            // Retrieve navigation controller
            let navController = splitViewController!.viewControllers.last! as! UINavigationController
            
            // Distinguish whether top controller is edit category controller class
            let topView = navController.topViewController!
            if topView.isKindOfClass(EditCategoryController) {
                let topViewController = topView as! EditCategoryController
                // If the top view is displaying current selected reminder, do not change top view
                if topViewController.category != nil {
                    if topViewController.category! == categoryList![indexPath.row] {
                        return
                    }
                }
                // If it is displaying a edit category controller, pop this view
                navController.popViewControllerAnimated(true)
            } else if topView.isKindOfClass(PickUpLocationController) {
                let topViewController = topView as! PickUpLocationController
                if topViewController.category != nil {
                    // If the top view is displaying the location info for current location, do not change
                    if topViewController.category! == categoryList![indexPath.row] {
                        return
                    }
                }
                // Pop current pickuplocation view controller
                navController.popViewControllerAnimated(true)
                // Pop editcategory view controller
                navController.popViewControllerAnimated(true)
            }
            
            // Instanciate the destination controller
            let destination = storyboard?.instantiateViewControllerWithIdentifier("EditCategoryController") as! EditCategoryController
            destination.delegate = self
            destination.isEditingExistingCategory = true
            let index = tableView.indexPathForSelectedRow!.row
            destination.category = categoryList![index]
            destination.categoryIndex = index
            
            // Push the new view to detail view
            navController.pushViewController(destination, animated: true)
            
            if UIDevice().userInterfaceIdiom == .Pad {
                // Hide master view
                let customSplitViewController = splitViewController! as! SplitViewController
                customSplitViewController.toggleMasterView()
            }
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        // Judge whether current segue should be performed or not
        if identifier == "EditCategory" || identifier == "AddCategory" {
            return false
        } else {
            return !isEditingMode
        }
    }
    
     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "AddCategory" {
            let destination = segue.destinationViewController as! EditCategoryController
            destination.delegate = self
            destination.isEditingExistingCategory = false
            destination.category = nil
            destination.categoryIndex = categoryList!.count
            
        }
        if segue.identifier == "EditCategory" {
            let destination = segue.destinationViewController as! EditCategoryController
            destination.delegate = self
            destination.isEditingExistingCategory = true
            let index = tableView.indexPathForSelectedRow!.row
            destination.category = categoryList![index]
            destination.categoryIndex = index
        }
        
        if segue.identifier == "ViewReminderList" {
            let destination = segue.destinationViewController as! ReminderListController
            let index = tableView.indexPathForSelectedRow!.row
            destination.reminders = Array(categoryList![index].reminders!)
            destination.category = categoryList![index]
        }
        
    }
    

}
