//
//  EditCategoryController.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit

protocol EditCategoryControllerDelegate {
    func categoryUpdated(sender: EditCategoryController, didFinishEditing existingCategory: Category)
    func categoryUpdated(sender: EditCategoryController, didFinishCreating newCategory: Category)
}

class EditCategoryController: UIViewController, PickUpLocationControllerDelegate {

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var colorSegment: UISegmentedControl!
    @IBOutlet weak var pickUpLocationButton: UIButton!
    @IBOutlet weak var needNotificationSwitch: UISwitch!
    @IBOutlet weak var notificationRangeSegment: UISegmentedControl!
    
    // A reference to AppDelegate
    let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // Delegate for receiving updates
    var delegate: EditCategoryControllerDelegate?
    
    // Variables for editing existing category
    var isEditingExistingCategory: Bool = false
    var category: Category?
    
    // We need index to create new category
    var categoryIndex: Int = 0
    
    // Store picked up location
    var location: Location?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialise the buttons
        setUpButtonEvents()
        
        // Based on whether is editing existing category 
        if isEditingExistingCategory {
            loadContent()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Custom Functions: Content Control
    
    func loadContent() {
        if category != nil {
            
            // Set title
            titleField.text = category!.title
            
            // Set title color
            let titleColor = TitleColor(rawValue: category!.color!)!
            colorSegment.selectedSegmentIndex = titleColorToIndex(titleColor)
            
            // Set address
            pickUpLocationButton.setTitle(category!.location!.address!, forState: UIControlState.Normal)
            
            // Set need notification switch
            switch NSManagedBool(rawValue: category!.needNotification! as Int)! {
            case .True: needNotificationSwitch.on = true
            case .False: needNotificationSwitch.on = false
            }
            
            // Set notification range
            let notificationRange = NotificationRange(rawValue: category!.notificationRange! as Int)!
            notificationRangeSegment.selectedSegmentIndex = notificationRangeToIndex(notificationRange)
            
            // We need to manually set the location for existing category, so that we can pass stored location directly
            location = category!.location!
        }
    }
    
    func titleColorToIndex(color: TitleColor) -> Int {
        switch color {
        case .Black: return 0
        case .Cyan: return 1
        case .Brown: return 2
        case .Red: return 3
        case .Green: return 4
        }
    }
    
    func IndexToTitleColor(index: Int) -> TitleColor {
        switch index {
        case 0: return .Black
        case 1: return .Cyan
        case 2: return .Brown
        case 3: return .Red
        case 4: return .Green
        default: return .Black
        }
    }
    
    func notificationRangeToIndex(range: NotificationRange) -> Int {
        switch range {
        case .Near: return 0
        case .Moderate: return 1
        case .Far: return 2
        }
    }
    
    func indexToNotificationRange(index: Int) -> NotificationRange {
        switch index {
        case 0: return .Near
        case 1: return .Moderate
        case 2: return .Far
        default: return .Near
        }
    }
    
    func inputValidation() -> UIAlertController? {
        var checkNotPass = false
        var message = "You must fill up "
        if titleField.text! == "" {
            message += "title, "
            checkNotPass = true
        }
        if colorSegment.selectedSegmentIndex == UISegmentedControlNoSegment {
            message += "title color, "
            checkNotPass = true
        }
        if location == nil {
            message += "location, "
            checkNotPass = true
        }
        if notificationRangeSegment.selectedSegmentIndex == UISegmentedControlNoSegment {
            message += "notification range, "
            checkNotPass = true
        }
        if checkNotPass {
            // Remove the ", " in the end of string
            message.removeRange(message.endIndex.advancedBy(-2)..<message.endIndex)
            return ViewController.getAlertControlloer("Error", message: message)
        } else {
            return nil
        }
    }
    
    // MARK: Custom Functions: Button Events

    func setUpButtonEvents() {
        // Set up buttons by assigning corresponding actions
        saveButton.target = self
        saveButton.action = #selector(saveButtonClicked)
        
        cancelButton.target = self
        cancelButton.action = #selector(cancelButtonClicked)
    }
    
    func saveButtonClicked() {
        // Do the input validation first
        let inputValidationAlert = inputValidation()
        if inputValidationAlert != nil {
            presentViewController(inputValidationAlert!, animated: true, completion: nil)
            return
        }
        
        // Use variables to store value
        let title = titleField.text!
        let color = IndexToTitleColor(colorSegment.selectedSegmentIndex)
        var needNotification: NSManagedBool
        if needNotificationSwitch.on {
            needNotification = NSManagedBool.True
        } else {
            needNotification = NSManagedBool.False
        }
        let notificationRange = indexToNotificationRange(notificationRangeSegment.selectedSegmentIndex)
        
        
        // Judge whether we are editing existing category
        if isEditingExistingCategory {
            
            // We do not retrieve location here, the location is updated by another delegate
            category!.title = title
            category!.color = color.rawValue
            category!.needNotification = needNotification.rawValue
            category!.notificationRange = notificationRange.rawValue
            category!.location = location!
            
            // Call delegate function
            delegate!.categoryUpdated(self, didFinishEditing: category!)
            
        } else {
            
            // Create new category
            let newCategory = CategoryListController.createNewCategory(appDelegate, index: categoryIndex, title: title, color: color, needNotification: needNotification, notificationRange: notificationRange, location: location!)
            
            // Call delegate function
            delegate!.categoryUpdated(self, didFinishCreating: newCategory)
        }
        
        navigationController?.popViewControllerAnimated(true)
    }
    
    func cancelButtonClicked() {
        // Go back to previous view
        navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - Delegate Functions: Delegate for PickUpLocationController
    
    func locationPicked(sender: PickUpLocationController, didPickUpNewLocation location: Location) {
        self.location = location
        pickUpLocationButton.setTitle(location.address!, forState: UIControlState.Normal)
    }
    
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PickUpLocation" {
            let destination = segue.destinationViewController as! PickUpLocationController
            destination.locationPickedDelegate = self
            if isEditingExistingCategory {
                destination.isEditingExistingLocation = true
                destination.location = location!
                destination.category = category!
            } else {
                destination.isEditingExistingLocation = false
                destination.location = nil
                destination.category = nil
            }
        }
    }

}
