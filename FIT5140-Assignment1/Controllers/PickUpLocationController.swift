//
//  PickUpLocationController.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit
import MapKit
import CoreData

protocol PickUpLocationControllerDelegate {
    func locationPicked(sender: PickUpLocationController, didPickUpNewLocation location: Location)
}

protocol SearchResultUpdaterDelegate {
    func searchResult(sender: PickUpLocationController, searchResultDidLoaded results: [GeocodeAddress])
}

class PickUpLocationController: UIViewController, MKMapViewDelegate, LocationUpdatedDelegate, UISearchResultsUpdating, SearchResultsControllerDelegate {

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var mapView: MKMapView!
    
    // A reference to AppDelegate
    var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // Delegate to receive location updating signals
    var locationPickedDelegate: PickUpLocationControllerDelegate?
    
    // Delegate to receive search result updating signals
    var searchResultDelegate: SearchResultUpdaterDelegate?
    
    // Variables for editing existing location
    var isEditingExistingLocation: Bool = false
    var location: Location?
    
    // Current editing category, this is used to controll navigations
    var category: Category?
    
    // The annotation used to show the target location to user
    var currentAnnotation: MKPointAnnotation!
    var annotationHasBeenAdded: Bool = false
    
    // The selected location
    var selectedLocation: GeocodeAddress!
    
    // Programmatically set searchbar because xcode does not support UISearchController
    var searchController: UISearchController!
    
    // Search Result Controller which will be used to display search result
    var searchResultsController: SearchResultsController!
    
    // The geocoder is used to translate address to coordinate and the other way around
    let geocoder: CLGeocoder = CLGeocoder()
    
    // This variable is used to determine whether we should change map view to current location
    var isInit = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set self as mapview delegate
        mapView.delegate = self
        
        // Add self to location manager delegates
        appDelegate.registerLocationUpdateDelegate(self)
        
        // Instantiate SearchResultController
        searchResultsController = storyboard?.instantiateViewControllerWithIdentifier("SearchResultsController") as! SearchResultsController
        searchResultsController.delegate = self
        
        // Assign delegates of search result updating
        searchResultDelegate = searchResultsController
        
        // Create and setup UISearchController
        searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchResultsUpdater = self
        
        // We want the view controller to be able to present search results
        self.definesPresentationContext = true
        
        // Set naviation title to search bar
        navigationItem.titleView = searchController.searchBar
        
        // Initialise button events
        setUpButtonEvents()
        
        // Load existing location info
        if isEditingExistingLocation {
            isInit = false
            loadLocationInfo()
        }
        
        // We do not need to show user's location
        mapView.showsUserLocation = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custon Functions: Manipulate data
    
    func loadLocationInfo() {
        // Based on the passing in location, create selectedLocation
        let coordinate = CLLocationCoordinate2D(latitude: location!.latitude as! Double, longitude: location!.longitude as! Double)
        selectedLocation = GeocodeAddress(coordinate: coordinate, address: location!.address!)
        
        // Modify annotation based on the information given by location
        modifyAnnotation(coordinate, title: location!.address!)
        
        // Add annotation to map if it has not been added
        if !annotationHasBeenAdded {
            annotationHasBeenAdded = true
            mapView.addAnnotation(currentAnnotation)
        }
        
        // Change map view point to passing in location
        changeRegion(coordinate)
    }
    
    // MARK: - Custom Functions: Map Controlling
    
    func modifyAnnotation(coordinate: CLLocationCoordinate2D, title: String) {
        if currentAnnotation == nil {
            currentAnnotation = MKPointAnnotation()
        }
        currentAnnotation.coordinate = coordinate
        currentAnnotation.title = title
    }
    
    func changeRegion(coordinate: CLLocationCoordinate2D) {
        // Change current view point to specific coordinate
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(coordinate, span)
        mapView.setRegion(region, animated: true)
    }
    
    // MARK: - Custom Functions: View Controll
    
    func setUpButtonEvents() {
        // Set up the button events
        saveButton.target = self
        saveButton.action = #selector(saveButtonClicked)
    }
    
    func saveButtonClicked() {
        // Input validdation
        if selectedLocation != nil {
            
            // Generate and store new location
            let newLocation = PickUpLocationController.createLocation(appDelegate, address: selectedLocation!.address, latitude: selectedLocation!.coordinate.latitude, longitude: selectedLocation!.coordinate.longitude)
            locationPickedDelegate!.locationPicked(self, didPickUpNewLocation: newLocation)
            navigationController?.popViewControllerAnimated(true)
        } else {
            
            // Generate alerts
            let alert = ViewController.getAlertControlloer("Error", message: "You must select a location!")
            presentViewController(alert, animated: true, completion: nil)
            
        }
    }
    
    // Mark: - Custom Functions: Data Manipulating
    
    static func createLocation(appDelegate: AppDelegate, address: String, latitude: Double, longitude: Double) -> Location {
        // Return a new location based on settings
        let newLocation = NSEntityDescription.insertNewObjectForEntityForName("Location", inManagedObjectContext: appDelegate.managedObjectContext)
        newLocation.setValue(address, forKey: "address")
        newLocation.setValue(latitude, forKey: "latitude")
        newLocation.setValue(longitude, forKey: "longitude")
        return newLocation as! Location
    }

    // MARK: - Delegate Functions: SearchControllerUpdater Delegate Functions
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        // Convert the address to coordinate
        geocoder.geocodeAddressString(searchController.searchBar.text!) { (marks: [CLPlacemark]?, error: NSError?) in
            
            if marks != nil {
                
                var searchResults = [GeocodeAddress]()
                for mark in marks! {
                    if mark.addressDictionary == nil || mark.location == nil {
                        continue
                    }
                    
                    // Convert the address dictionary to address string
                    let addressLines = mark.addressDictionary!["FormattedAddressLines"] as! [String]
                    var address = ""
                    for line in addressLines {
                        address += line
                        if addressLines.last! != line {
                            address += ", "
                        }
                    }
                    
                    // Generate GeocodeAddress and add it to search result
                    searchResults.append(GeocodeAddress(coordinate: mark.location!.coordinate, address: address))
                }
                
                // Notify search result updater delegate to update results
                self.searchResultDelegate!.searchResult(self, searchResultDidLoaded: searchResults)
            }
        }
    }
    
    // MARK: - Delegate Functions: Search Results Controller Delegate
    
    func searchResult(sender: SearchResultsController, didSelectResult result: GeocodeAddress) {
        selectedLocation = result
        modifyAnnotation(selectedLocation.coordinate, title: selectedLocation.address)
        if !annotationHasBeenAdded {
            mapView.addAnnotation(currentAnnotation)
        }
        changeRegion(selectedLocation.coordinate)
        searchController.active = false
        searchController.searchBar.text = selectedLocation.address
    }
    
    // MARK: - Delegate Functions: Mapview delegate
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
        view.animatesDrop = true
        view.draggable = true
        return view
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, didChangeDragState newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        if newState == MKAnnotationViewDragState.Ending {
            // Get new coordinate
            let newCoordinate = view.annotation!.coordinate
            
            // Generate location based on coordinate
            let newLocation = CLLocation(coordinate: newCoordinate, altitude: 0, horizontalAccuracy: 5, verticalAccuracy: 5, timestamp: NSDate())
            
            // Resolve coordinate to actual address
            geocoder.reverseGeocodeLocation(newLocation, completionHandler: { (marks: [CLPlacemark]?, error: NSError?) in
                let addressLines = marks!.first!.addressDictionary!["FormattedAddressLines"] as! [String]
                var address = ""
                for line in addressLines {
                    address += line
                    if addressLines.last != line {
                        address += ", "
                    }
                }
                // After retrieved results, set address to search bar
                self.searchController.searchBar.text = address
                // Change selectedLocation to corresponding coordinate and address
                self.selectedLocation = GeocodeAddress(coordinate: newCoordinate, address: address)
            })
        }
    }
    
    // MARK: - Delegate Functions: Location Updated Delegate
    
    func locationUpdated(sender: AppDelegate, locationIsUpdated location: CLLocation) {
        if isInit {
            modifyAnnotation(location.coordinate, title: "Current Location")
            changeRegion(location.coordinate)
            isInit = false
            if !annotationHasBeenAdded {
                mapView.addAnnotation(currentAnnotation)
                annotationHasBeenAdded = true
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    }*/

}
