//
//  ReminderListController.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit
import CoreData

protocol ReminderStateChangedDelegate {
    func reminderStateChanged(sender: ReminderListController, reminderStateIsChanged newState: Bool)
}

class ReminderListController: UITableViewController, ReminderCellDelegate, EditReminderControllerDelegate {

    @IBOutlet weak var addButton: UIBarButtonItem!
    
    // Create a reference to AppDelegate
    var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // Passing in reminder list
    var reminders: [Reminder]?
    
    // The category reminders belongs to
    var category: Category?
    
    // This delegate is used for view controllers who wants to receive the update event about reminder states
    var delegate: ReminderStateChangedDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // We want the list to be sorted before it is presented
        sortList()
        
        // Set up event for add button
        addButton.target = self
        addButton.action = #selector(addButtonClicked)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        // If I was poped out, remove all the detail views added by me
        if navigationController?.viewControllers.indexOf(self) == nil {
            // Retrieve navigation controller of detail view
            let navController = splitViewController!.viewControllers.last! as! UINavigationController
            
            // Pop out all the view until map view is the first view
            for view in navController.viewControllers {
                if !view.isKindOfClass(MapController) {
                    navController.popViewControllerAnimated(true)
                }
            }
        }
    }
    
    // MARK: Custom Functions: Button Event
    
    func addButtonClicked() {
        // Retrieve navigation controller of detail view
        let navController = splitViewController!.viewControllers.last! as! UINavigationController
        
        let topView = navController.topViewController!
        if topView.isKindOfClass(EditReminderController) {
            let topViewController = topView as! EditReminderController
            // If the top view controller is displaying a view for insert reminder, change nothing
            if !topViewController.isEditingExistingReminder {
                return
            }
            // Pop edit reminder controller view
            navController.popViewControllerAnimated(true)
        }
        
        // Instanciate destination by identifier
        let destination = storyboard?.instantiateViewControllerWithIdentifier("EditReminderController") as! EditReminderController
        destination.delegate = self
        destination.isEditingExistingReminder = false
        destination.reminder = nil
        
        // Push destination to navigation controller
        navController.pushViewController(destination, animated: true)
        
        if UIDevice().userInterfaceIdiom == .Pad {
            // Hide master view
            let customSplitViewController = splitViewController! as! SplitViewController
            customSplitViewController.toggleMasterView()
        }
    }
    
    // MARK: Custom Functions: Data Manipulating
    
    static func createReminder(appDelegate: AppDelegate, title: String, finished: NSManagedBool, dueDate: NSDate?, note: String?) -> Reminder {
        // Return a new reminder based on settings
        let newReminder = NSEntityDescription.insertNewObjectForEntityForName("Reminder", inManagedObjectContext: appDelegate.managedObjectContext)
        newReminder.setValue(title, forKey: "title")
        newReminder.setValue(finished.rawValue, forKey: "finished")
        newReminder.setValue(dueDate, forKey: "dueDate")
        newReminder.setValue(note, forKey: "note")
        return newReminder as! Reminder
    }
    
    func sortList() {
        // Sort the reminder list
        var sortedList = [Reminder]()
        sortedList = self.reminders!.sort({ (reminder1: Reminder, reminder2: Reminder) -> Bool in
            if reminder1.finished == 1 && reminder2.finished == 0 {
                return false
            }
            if reminder1.finished == 0 && reminder2.finished == 1 {
                return true
            }
            if reminder1.finished == reminder2.finished {
                if reminder1.dueDate == nil && reminder2.dueDate == nil {
                    return true
                }
                if reminder1.dueDate != nil && reminder2.dueDate != nil {
                    let result = reminder1.dueDate!.compare(reminder2.dueDate!)
                    if result == NSComparisonResult.OrderedAscending || result == NSComparisonResult.OrderedSame {
                        return true
                    } else {
                        return false
                    }
                }
                if reminder1.dueDate == nil && reminder2.dueDate != nil {
                    return false
                }
                if reminder1.dueDate != nil && reminder2.dueDate == nil {
                    return true
                }
            }
            return true
        })
        self.reminders = sortedList
    }
    
    // MARK: - Delegate Functions: Reminder Cell Delegate
    
    func stateChanged(sender: ReminderCell, stateDidChanged newState: Bool, reminder: Reminder) {
        if newState {
            reminder.finished = NSManagedBool.True.rawValue
        } else {
            reminder.finished = NSManagedBool.False.rawValue
        }
        appDelegate.saveContext()
        sortList()
        tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
        if delegate != nil {
            delegate.reminderStateChanged(self, reminderStateIsChanged: newState)
        }
    }
    
    // MARK: - Delegate Functions: Reminder is edited
    
    func reminderUpdated(sender: EditReminderController, reminderIsUpdated existingReminder: Reminder) {
        appDelegate.saveContext()
        sortList()
        tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
    }
    
    func reminderUpdated(sender: EditReminderController, reminderIsCreated newReminder: Reminder) {
        category!.reminders!.insert(newReminder)
        appDelegate.saveContext()
        reminders!.append(newReminder)
        sortList()
        tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reminders!.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ReminderCell", forIndexPath: indexPath) as! ReminderCell

        // First we have to rest cells to normal state
        cell.titleLabel.attributedText = nil
        cell.titleLabel.textColor = UIColor.blackColor()
        
        // Configure the cell based on the specification
        let reminder = reminders![indexPath.row]
        cell.titleLabel.text = reminder.title
        if reminder.dueDate != nil && reminder.finished != 1 {
            let comparison = reminder.dueDate!.compare(NSDate())
                
            if comparison == NSComparisonResult.OrderedAscending {
                cell.titleLabel!.textColor = UIColor.redColor()
            }
        }
        
        // Set the finished state and add removing line for finished reminder
        if reminder.finished == NSManagedBool.False.rawValue {
            cell.finishedSwitch.on = false
        } else {
            cell.finishedSwitch.on = true
            let attributedString = NSMutableAttributedString(string: reminder.title!, attributes: [NSStrikethroughStyleAttributeName: NSNumber(integer: NSUnderlineStyle.StyleSingle.rawValue)])
                cell.titleLabel.attributedText = attributedString
        }
        
        // Add delegate and working object to cell
        cell.delegate = self
        cell.reminder = reminder

        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // All rows should be editable
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            appDelegate.managedObjectContext.deleteObject(reminders![indexPath.row])
            appDelegate.saveContext()
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }

    // MARK: - Navigation

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        return false
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Retrieve navigation controller
        let navController = splitViewController!.viewControllers.last! as! UINavigationController
        
        // Distinguish whether top controller is edit category controller class
        let topView = navController.topViewController!
        if topView.isKindOfClass(EditReminderController) {
            let topViewController = topView as! EditReminderController
            // If the top view is displaying current selected reminder, do not change top view
            if topViewController.reminder != nil {
                if topViewController.reminder! == reminders![indexPath.row] {
                    return
                }
            }
            // If it is displaying a editcategorycontroller, pop this view
            navController.popViewControllerAnimated(true)
        }
    
        // Instanciate destination by identifier
        let destination = storyboard?.instantiateViewControllerWithIdentifier("EditReminderController") as! EditReminderController
        destination.delegate = self
        destination.isEditingExistingReminder = true
        destination.reminder = reminders![indexPath.row]
        
        // Set up the delegate to receive update state changes
        delegate = destination
        
        // Push destination to navigation controller
        navController.pushViewController(destination, animated: true)
        
        if UIDevice().userInterfaceIdiom == .Pad {
            // Hide master view
            let customSplitViewController = splitViewController! as! SplitViewController
            customSplitViewController.toggleMasterView()
        }
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "AddReminder" {
            let destination = segue.destinationViewController as! EditReminderController
            destination.delegate = self
            destination.isEditingExistingReminder = false
            destination.reminder = nil
            
        }
        
        if segue.identifier == "EditReminder" {
            let destination = segue.destinationViewController as! EditReminderController
            destination.delegate = self
            destination.isEditingExistingReminder = true
            destination.reminder = reminders![tableView.indexPathForSelectedRow!.row]
        }
    }

}
