//
//  ReminderCell.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit

protocol ReminderCellDelegate {
    func stateChanged(sender: ReminderCell, stateDidChanged newState: Bool, reminder: Reminder)
}

class ReminderCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var finishedSwitch: UISwitch!
    
    // The delegate which receives the state changing signal
    var delegate: ReminderCellDelegate?
    
    // The object which displayed by the cell
    var reminder: Reminder?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // We want to be notified when the finished state is changed
        finishedSwitch.addTarget(self, action: #selector(stateChanged), forControlEvents: UIControlEvents.ValueChanged)
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func stateChanged() {
        delegate!.stateChanged(self, stateDidChanged: finishedSwitch.on, reminder: reminder!)
    }
    
}
