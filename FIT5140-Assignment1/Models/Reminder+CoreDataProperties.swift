//
//  Reminder+CoreDataProperties.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Reminder {

    @NSManaged var title: String?
    @NSManaged var note: String?
    @NSManaged var dueDate: NSDate?
    @NSManaged var finished: NSNumber?
    @NSManaged var owner: Category?

}
