//
//  GeocodeAddress.swift
//  FIT5140-Assignment1
//
//  Created by Daniel Liu on 8/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import CoreLocation

class GeocodeAddress : NSObject {
    var coordinate: CLLocationCoordinate2D!
    var address: String!
    
    init(coordinate: CLLocationCoordinate2D, address: String) {
        self.coordinate = coordinate
        self.address = address
    }
    
}